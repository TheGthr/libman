# LibMan

LibMan is a Golang app based on cobra that helps managing media libraries.

## Movies

Usage: `libman movies <path>`

Loop through each file in the directory (and subdirectories) given as an argument and checks if it is a video file.
If it is, it gets the movie title from the filename and searches for the movie on TMDb. It then retrieves the movie details and formats the new filename according to the following pattern:
`TITLE (YEAR) [QUAL] [tmdbid-TMDBID].EXT`. With:

- `TITLE`: title of the movie in its original version (i.e. in English if the movie is in English, French if it is a French movie...)
- `YEAR`: year of release of the movie
- `QUAL`: quality of the movie, either 420p, 720p, 1080p, 2K, 4K or Other
- `TMDBID`: the TMDb ID of the movie
- `EXT`: the original extension of the file

The choice of the filename is let to the user, a report file of all the renamed or skipped files will also be generated.