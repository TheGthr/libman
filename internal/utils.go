package internal

import "path/filepath"

// isVideoFile checks if a file is a video file based on its extension
func IsVideoFile(path string) bool {
	// Define the video file extensions
	videoExts := []string{".mp4", ".mkv", ".avi", ".mov", ".wmv", ".flv"}

	// Get the file extension
	ext := filepath.Ext(path)

	// Check if the extension is in the video file extensions
	for _, videoExt := range videoExts {
		if ext == videoExt {
			return true
		}
	}
	return false
}
