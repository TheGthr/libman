package internal

import (
	"errors"
	"fmt"
	"gopkg.in/yaml.v3"
	"os"
	"time"
)

// Report represents the report of the renaming process
type Report struct {
	Renamed []string `yaml:"renamed"`
	Skipped []string `yaml:"skipped"`
	Errors  []string `yaml:"errors"`
}

// WriteReport writes the report to a yaml file
func (r *Report) WriteReport(filename string) error {
	// Check if file already exists
	if _, err := os.Stat(filename); !errors.Is(err, os.ErrNotExist) {
		filename = fmt.Sprintf("%s-%s-%d:%d", filename, time.DateOnly, time.Now().Minute(), time.Now().Second())
	}
	// Create a new yaml file
	file, err := os.Create(fmt.Sprintf("%s.yaml", filename))
	if err != nil {
		return err
	}
	defer file.Close()

	// Marshal the report struct into yaml format
	data, err := yaml.Marshal(r)
	if err != nil {
		return err
	}

	// Write the yaml data to the file
	_, err = file.Write(data)
	if err != nil {
		return err
	}

	// Return nil if no errors
	return nil
}
