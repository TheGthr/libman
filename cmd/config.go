package cmd

import (
	"errors"
	"fmt"
	"github.com/manifoldco/promptui"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"log"
)

// ConfigCmd is the command for setting the API key
var configCmd = &cobra.Command{
	Use:   "config",
	Short: "Set the API key for TMDb",
	Run: func(cmd *cobra.Command, args []string) {
		// Prompt the user to enter the API key
		prompt := promptui.Prompt{
			Label: "Enter the API key for TMDb",
			Mask:  ' ',
		}

		// Get the user's input
		result, err := prompt.Run()
		if err != nil {
			log.Fatal(err)
		}

		// Set the API key in the viper config
		viper.Set("apikey", string(result))

		// Write the config to a file
		err = viper.WriteConfig()
		if err != nil {
			log.Fatal(err)
		}

		fmt.Println("API key set successfully")
	},
}

func init() {
	rootCmd.AddCommand(configCmd)

	// Set the config file name and type
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")

	// Add the current directory as a config path
	viper.AddConfigPath(".")

	// Read the config file
	err := viper.ReadInConfig()
	if err != nil {
		// If the config file does not exist, create a new one
		var configFileNotFoundError viper.ConfigFileNotFoundError
		if errors.As(err, &configFileNotFoundError) {
			err = viper.SafeWriteConfig()
			if err != nil {
				log.Fatal(err)
			}
		}
	}

	// Get the API key from the config
	APIKey = viper.GetString("apikey")
}
