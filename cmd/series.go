package cmd

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/manifoldco/promptui"
	"github.com/spf13/cobra"
	"libman/internal"
	"log"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"regexp"
	"strings"
)

// Series represents a series from TMDb
type Series struct {
	ID       int    `json:"id"`
	Title    string `json:"title"`
	Original string `json:"original_title"`
	Lang     string `json:"original_language"`
	Year     string `json:"release_date"`
}

// SearchResponseSeries represents the response from TMDb search API
type SearchResponseSeries struct {
	Results []Series `json:"results"`
}

// RootCmd is the root command for the app
var seriesCmd = &cobra.Command{
	Use:   "series",
	Short: "Rename series files based on TMDb",
	Run: func(cmd *cobra.Command, args []string) {
		// TODO: Only rename directory with Name and tmdbid, create subdirs with tmdbids and rename everything in them as S0AE0B

		report := &internal.Report{}

		// Check if the API key is set
		if APIKey == "" {
			fmt.Println("Please set the API key using the config command")
			return
		}

		// Check if the directory path is provided
		if len(args) == 0 {
			fmt.Println("Please provide the directory path where movie files are located")
			return
		}

		// Get the directory path
		dir := args[0]

		// Walk through the directory and subdirectories
		err := filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}

			// Check if the file is a video file
			if internal.IsVideoFile(path) {
				// Get the movie title from the filename
				title := getMovieTitle(path)

				// Search for the movie on TMDb
				movies, err := searchSeries(title)
				if err != nil {
					return err
				}

				// If no results are found, skip the file
				if len(movies) == 0 {
					fmt.Printf("No results found for %s\n", title)
					report.Errors = append(report.Errors, path)
					return nil
				}

				// Format the new filenames according to the specified pattern
				newNames := formatNewSeriesNames(movies, path)

				// Add the original filename as an option
				newNames = append(newNames, filepath.Base(path))

				// Prompt the user to choose between the original filename and the new names
				prompt := promptui.Select{
					Label: fmt.Sprintf("Choose a new name for %s", path),
					Items: newNames,
				}
				// Get the user's choice
				_, result, err := prompt.Run()
				if err != nil {
					report.Errors = append(report.Errors, path)
					return err
				}

				// If the user chose a different name, rename the file
				if result != filepath.Base(path) {
					newPath := filepath.Join(filepath.Dir(path), result)
					err = os.Rename(path, newPath)
					if err != nil {
						return err
					}
					fmt.Printf("Renamed %s to %s\n", path, newPath)
					report.Renamed = append(report.Renamed, fmt.Sprintf("%s -> %s", path, newPath))
				} else {
					fmt.Printf("Skipped %s\n", path)
					report.Skipped = append(report.Skipped, path)
				}
			}
			err = report.WriteReport("report")
			if err != nil {
				log.Fatal(err)
			}
			return nil
		})
		if err != nil {
			log.Fatal(err)
		}
	},
}

// getSeriesTitle extracts the movie title from the filename
func getSeriesTitle(path string) (string, string) {
	// Get the file name without the extension
	name := strings.TrimSuffix(filepath.Base(path), filepath.Ext(path))

	year := ""
	// Get release date if it exists
	if s := regexp.MustCompile(`\([^)]*\)`).FindStringSubmatch(name); len(s) > 0 {
		year = s[0]
	}

	// Remove any brackets and their contents, and dates
	name = regexp.MustCompile(`\[[^]]*]`).ReplaceAllString(name, "")
	name = regexp.MustCompile(`\([^)]*\)`).ReplaceAllString(name, "")
	name = regexp.MustCompile(`\d{4}`).ReplaceAllString(name, "")

	// Replace any dots or underscores with spaces
	name = strings.ReplaceAll(name, ".", " ")
	name = strings.ReplaceAll(name, "_", " ")
	name = strings.ReplaceAll(name, "720p", "")
	name = strings.ReplaceAll(name, "1080p", "")

	// Trim any leading or trailing spaces
	name = strings.TrimSpace(name)

	// Remove date
	name = regexp.MustCompile(`\d{4}$`).ReplaceAllString(name, "")

	// Trim any leading or trailing spaces
	name = strings.TrimSpace(name)
	name = strings.Split(name, "  ")[0]

	return name, year
}

// searchMovie searches for a movie on TMDb based on its title
func searchSeries(title string) ([]Series, error) {
	if APIKey == "" {
		return nil, errors.New("no APIKey found")
	}
	// Define the TMDb search API endpoint
	endpoint := "https://api.themoviedb.org/3/search/tv"

	// Create a URL query with the API key and the title
	query := url.Values{}
	query.Add("api_key", APIKey)
	query.Add("query", title)

	// Append the query to the endpoint
	endpoint = endpoint + "?" + query.Encode()

	// Make a GET request to the endpoint
	resp, err := http.Get(endpoint)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	// Decode the response body into a SearchResponse struct
	var searchResponse SearchResponseSeries
	err = json.NewDecoder(resp.Body).Decode(&searchResponse)
	if err != nil {
		return nil, err
	}

	// Return the results as a slice of Movie structs
	return searchResponse.Results, nil
}

// formatNewSeriesNames formats the new filenames according to the specified pattern
func formatNewSeriesNames(movies []Series, path string) []string {
	// Define a slice to store the new names
	var newNames []string

	// Get the quality and the extension from the original filename and ffprobe details
	qual := getVideoResolution(path)
	ext := filepath.Ext(path)

	// Loop through the movies
	for _, movie := range movies {
		// Get the title, year and tmdbid from the movie
		title := ""
		if movie.Lang == "fr" || movie.Lang == "en" || movie.Lang == "es" || movie.Lang == "it" {
			title = movie.Original
		} else {
			title = movie.Title
		}

		year := ""
		if movie.Year != "" {
			year = movie.Year[:4]
		}
		tmdbid := movie.ID

		// Format the new name according to the pattern
		newName := fmt.Sprintf("%s (%s) [%s] [tmdbid-%d]%s", title, year, qual, tmdbid, ext)

		// Append the new name to the slice
		newNames = append(newNames, newName)
	}

	// Return the slice of new names
	return newNames
}

// getVideoResolution gets the resolution of a movie file based on ffprobe details
/*func getVideoResolution(path string) string {
	ctx, cancelFn := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancelFn()

	data, err := ffprobe.ProbeURL(ctx, path)
	if err != nil {
		return "Unknown"
	}

	line := data.FirstVideoStream()
	// Define the thresholds for 480p, 720p, 1080p, 2K and 4K labels
	// Assuming that the aspect ratio is between 16:9 and 21:9 and the tolerance is 5%
	if line.Width >= 3648 && line.Height >= 1536 {
		return "4K"
	} else if line.Width >= 2432 && line.Height >= 1024 {
		return "2K"
	} else if line.Width >= 1824 && line.Height >= 768 {
		return "1080p"
	} else if line.Width >= 1216 && line.Height >= 512 {
		return "720p"
	} else if line.Width >= 912 && line.Height >= 384 {
		return "480p"
	} else {
		return "Other"
	}
}*/

func init() {
	rootCmd.AddCommand(seriesCmd)
}
