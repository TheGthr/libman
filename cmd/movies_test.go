package cmd

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"
)

// TestIsVideoFile tests the isVideoFile function
func TestIsVideoFile(t *testing.T) {
	// Define the test cases
	testCases := []struct {
		path     string
		expected bool
	}{
		{"movie.mp4", true},
		{"movie.mkv", true},
		{"movie.avi", true},
		{"movie.mov", true},
		{"movie.wmv", true},
		{"movie.flv", true},
		{"movie.txt", false},
		{"movie.jpg", false},
		{"movie.pdf", false},
	}

	// Loop through the test cases
	for _, testCase := range testCases {
		// Call the isVideoFile function with the test case path
		actual := isVideoFile(testCase.path)

		// Check if the actual result matches the expected result
		if actual != testCase.expected {
			// If not, report an error
			t.Errorf("isVideoFile(%s) = %v; want %v", testCase.path, actual, testCase.expected)
		}
	}
}

// TestGetMovieTitle tests the getMovieTitle function
func TestGetMovieTitle(t *testing.T) {
	// Define the test cases
	testCases := []struct {
		path     string
		expected string
	}{
		{"The.Matrix.1999.1080p.mp4", "The Matrix"},
		{"The_Lord_of_the_Rings_The_Return_of_the_King_(2003).mkv", "The Lord of the Rings The Return of the King"},
		{"Inception [2010] 720p.avi", "Inception"},
		{"The Shawshank Redemption (1994).mov", "The Shawshank Redemption"},
		{"The Godfather [1972].wmv", "The Godfather"},
		{"The Dark Knight (2008) [1080p].flv", "The Dark Knight"},
	}

	// Loop through the test cases
	for _, testCase := range testCases {
		// Call the getMovieTitle function with the test case path
		actual := getMovieTitle(testCase.path)

		// Check if the actual result matches the expected result
		if actual != testCase.expected {
			// If not, report an error
			t.Errorf("getMovieTitle(%s) = %s; want %s", testCase.path, actual, testCase.expected)
		}
	}
}

// TestSearchMovie tests the searchMovie function
func TestSearchMovie(t *testing.T) {
	// Define the test cases
	testCases := []struct {
		title    string
		expected []Movie
	}{
		{"The Matrix", []Movie{{ID: 603, Title: "The Matrix", Original: "The Matrix", Year: "1999-03-30"}}},
		{"The Lord of the Rings The Return of the King", []Movie{{ID: 122, Title: "The Lord of the Rings: The Return of the King", Original: "The Lord of the Rings: The Return of the King", Year: "2003-12-01"}}},
		{"Inception", []Movie{{ID: 27205, Title: "Inception", Original: "Inception", Year: "2010-07-15"}}},
		{"The Shawshank Redemption", []Movie{{ID: 278, Title: "The Shawshank Redemption", Original: "The Shawshank Redemption", Year: "1994-09-23"}}},
		{"The Godfather", []Movie{{ID: 238, Title: "The Godfather", Original: "The Godfather", Year: "1972-03-14"}}},
		{"The Dark Knight", []Movie{{ID: 155, Title: "The Dark Knight", Original: "The Dark Knight", Year: "2008-07-16"}}},
	}

	// Loop through the test cases
	for _, testCase := range testCases {
		// Call the searchMovie function with the test case title
		actual, err := searchMovie(testCase.title)
		if err != nil {
			// If there is an error, report a fatal error
			t.Fatalf("searchMovie(%s) returned an error: %v", testCase.title, err)
		}

		// Check if the actual result matches the expected result
		if !equalMovies(actual, testCase.expected) {
			// If not, report an error
			t.Errorf("searchMovie(%s) = %v; want %v", testCase.title, actual, testCase.expected)
		}
	}
}

// equalMovies checks if two slices of movies are equal
func equalMovies(a, b []Movie) bool {
	// Check if the lengths are equal
	if len(a) != len(b) {
		return false
	}

	// Loop through the slices
	for i := range a {
		// Check if the movies at the same index are equal
		if a[i] != b[i] {
			return false
		}
	}

	// Return true if no differences are found
	return true
}

// TestFormatNewNames tests the formatNewNames function
func TestFormatNewNames(t *testing.T) {
	// Define the test cases
	testCases := []struct {
		movies   []Movie
		path     string
		expected []string
	}{
		{[]Movie{{ID: 603, Title: "The Matrix", Original: "The Matrix", Year: "1999-03-30"}}, "The.Matrix.1999.1080p.mp4", []string{"The Matrix (1999) [Unknown] [tmdbid-603].mp4"}},
		{[]Movie{{ID: 122, Title: "The Lord of the Rings: The Return of the King", Original: "The Lord of the Rings: The Return of the King", Year: "2003-12-01"}}, "The_Lord_of_the_Rings_The_Return_of_the_King_(2003).mkv", []string{"The Lord of the Rings: The Return of the King (2003) [Unknown] [tmdbid-122].mkv"}},
		{[]Movie{{ID: 27205, Title: "Inception", Original: "Inception", Year: "2010-07-15"}}, "Inception [2010] 720p.avi", []string{"Inception (2010) [Unknown] [tmdbid-27205].avi"}},
		{[]Movie{{ID: 278, Title: "The Shawshank Redemption", Original: "The Shawshank Redemption", Year: "1994-09-23"}}, "The Shawshank Redemption (1994).mov", []string{"The Shawshank Redemption (1994) [Unknown] [tmdbid-278].mov"}},
		{[]Movie{{ID: 238, Title: "The Godfather", Original: "The Godfather", Year: "1972-03-14"}}, "The Godfather [1972].wmv", []string{"The Godfather (1972) [Unknown] [tmdbid-238].wmv"}},
		{[]Movie{{ID: 155, Title: "The Dark Knight", Original: "The Dark Knight", Year: "2008-07-16"}}, "The Dark Knight (2008) [1080p].flv", []string{"The Dark Knight (2008) [Unknown] [tmdbid-155].flv"}},
	}

	// Loop through the test cases
	for _, testCase := range testCases {
		// Call the formatNewNames function with the test case movies and path
		actual := formatNewNames(testCase.movies, testCase.path)

		// Check if the actual result matches the expected result
		if !equalStrings(actual, testCase.expected) {
			// If not, report an error
			t.Errorf("formatNewNames(%v, %s) = %v; want %v", testCase.movies, testCase.path, actual, testCase.expected)
		}
	}
}

// equalStrings checks if two slices of strings are equal
func equalStrings(a, b []string) bool {
	// Check if the lengths are equal
	if len(a) != len(b) {
		return false
	}

	// Loop through the slices
	for i := range a {
		// Check if the strings at the same index are equal
		if a[i] != b[i] {
			return false
		}
	}

	// Return true if no differences are found
	return true
}

// TestGetVideoResolution tests the getVideoResolution function with different movie files
func TestGetVideoResolution(t *testing.T) {
	// Create a temporary directory to store the test files
	tmpDir, err := ioutil.TempDir("", "test")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(tmpDir) // clean up

	// Define the test cases with the expected resolution and the ffprobe output
	testCases := []struct {
		resolution string
		ffprobe    string
	}{
		{"4K", `Input #0, mov,mp4,m4a,3gp,3g2,mj2, from '4k.mp4':
  Metadata:
    major_brand     : isom
    minor_version   : 512
    compatible_brands: isomiso2avc1mp41
    encoder         : Lavf58.29.100
  Duration: 00:00:10.00, start: 0.000000, bitrate: 10188 kb/s
    Stream #0:0(und): Video: h264 (High) (avc1 / 0x31637661), yuv420p, 3840x2160 [SAR 1:1 DAR 16:9], 10003 kb/s, 30 fps, 30 tbr, 15360 tbn, 60 tbc (default)
    Metadata:
      handler_name    : VideoHandler`},
		{"2K", `Input #0, mov,mp4,m4a,3gp,3g2,mj2, from '2k.mp4':
  Metadata:
    major_brand     : isom
    minor_version   : 512
    compatible_brands: isomiso2avc1mp41
    encoder         : Lavf58.29.100
  Duration: 00:00:10.00, start: 0.000000, bitrate: 2545 kb/s
    Stream #0:0(und): Video: h264 (High) (avc1 / 0x31637661), yuv420p, 2560x1440 [SAR 1:1 DAR 16:9], 2360 kb/s, 30 fps, 30 tbr, 15360 tbn, 60 tbc (default)
    Metadata:
      handler_name    : VideoHandler`},
		{"1080p", `Input #0, mov,mp4,m4a,3gp,3g2,mj2, from '1080p.mp4':
  Metadata:
    major_brand     : isom
    minor_version   : 512
    compatible_brands: isomiso2avc1mp41
    encoder         : Lavf58.29.100
  Duration: 00:00:10.00, start: 0.000000, bitrate: 1273 kb/s
    Stream #0:0(und): Video: h264 (High) (avc1 / 0x31637661), yuv420p, 1920x1080 [SAR 1:1 DAR 16:9], 1138 kb/s, 30 fps, 30 tbr, 15360 tbn, 60 tbc (default)
    Metadata:
      handler_name    : VideoHandler`},
		{"720p", `Input #0, mov,mp4,m4a,3gp,3g2,mj2, from '720p.mp4':
  Metadata:
    major_brand     : isom
    minor_version   : 512
    compatible_brands: isomiso2avc1mp41
    encoder         : Lavf58.29.100
  Duration: 00:00:10.00, start: 0.000000, bitrate: 636 kb/s
    Stream #0:0(und): Video: h264 (High) (avc1 / 0x31637661), yuv420p, 1280x720 [SAR 1:1 DAR 16:9], 569 kb/s, 30 fps, 30 tbr, 15360 tbn, 60 tbc (default)
    Metadata:
      handler_name    : VideoHandler`},
		{"480p", `Input #0, mov,mp4,m4a,3gp,3g2,mj2, from '480p.mp4':
  Metadata:
    major_brand     : isom
    minor_version   : 512
    compatible_brands: isomiso2avc1mp41
    encoder         : Lavf58.29.100
  Duration: 00:00:10.00, start: 0.000000, bitrate: 318 kb/s
    Stream #0:0(und): Video: h264 (High) (avc1 / 0x31637661), yuv420p, 960x540 [SAR 1:1 DAR 16:9], 284 kb/s, 30 fps, 30 tbr, 15360 tbn, 60 tbc (default)
    Metadata:
      handler_name    : VideoHandler`},
		{"Unknown", `Input #0, mov,mp4,m4a,3gp,3g2,mj2, from 'unknown.mp4':
  Metadata:
    major_brand     : isom
    minor_version   : 512
    compatible_brands: isomiso2avc1mp41
    encoder         : Lavf58.29.100
  Duration: 00:00:10.00, start: 0.000000, bitrate: 159 kb/s
    Stream #0:0(und): Video: h264 (High) (avc1 / 0x31637661), yuv420p, 640x360 [SAR 1:1 DAR 16:9], 142 kb/s, 30 fps, 30 tbr, 15360 tbn, 60 tbc (default)
    Metadata:
      handler_name    : VideoHandler`},
	}

	// Loop through the test cases and create the test files with the ffprobe output
	for _, tc := range testCases {
		// Create a temporary file with the ffprobe output
		tmpFile, err := os.CreateTemp(tmpDir, "*.mp4")
		if err != nil {
			t.Fatal(err)
		}
		defer tmpFile.Close() // clean up

		// Write the ffprobe output to the file
		if _, err := tmpFile.WriteString(tc.ffprobe); err != nil {
			t.Fatal(err)
		}

		// Get the file name and path
		fileName := tmpFile.Name()
		filePath := filepath.Join(tmpDir, fileName)

		// Call the getVideoResolution function with the file path
		res := getVideoResolution(filePath)

		// Check if the result matches the expected resolution
		if res != tc.resolution {
			t.Errorf("getVideoResolution(%s) = %s; want %s", fileName, res, tc.resolution)
		}
	}
}
