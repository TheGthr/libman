package main

import (
	"libman/cmd"
)

func main() {
	// Execute the root command
	cmd.Execute()
}
